# EC2 CloudFormation template for Selenium Grid #

Setting up a [Selenium Grid](https://code.google.com/p/selenium/wiki/Grid2) on [Amazon EC2](https://aws.amazon.com/ec2/) is easy with this [CloudFormation](https://aws.amazon.com/documentation/cloudformation/) template.


### Usage ###

* Download: https://bitbucket.org/uitsweb/selenium-grid-ec2/raw/HEAD/selenium-grid.template
* In the CloudFormation area of your AWS console, create a new stack
* Provide a name
* Upload selenium-grid.template
* Click next and fill in remaining parameter data when prompted
* Launch
* When Status says "CREATE\_COMPLETE", click on the output tab to see the Selenium Grid console URL.  **Wait 10 minutes** for the console setup to complete before clicking the URL.
* **Wait up to 1 hour** for all the Selenium nodes to load in the console (there will be 4)


### License ###

[ECLv2](https://en.wikipedia.org/wiki/Educational_Community_License)

Copyright © Arizona Board of Regents
