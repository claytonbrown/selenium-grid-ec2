mkdir C:/selenium
netsh advfirewall set allprofiles state off
cscript C:\Windows\system32\scregedit.wsf /au 4
Restart-Service wuauserv
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "ConsentPromptBehaviorAdmin" -Value 00000000
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings" -Name "WarnOnZoneCrossing" -Value 00000000
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Stop-Process -Name Explorer -Force
cd C:\selenium
$webclient = New-Object System.Net.WebClient
$j = $webclient.DownloadString("https://www.java.com/en/download/manual.jsp")
$j -match "Offline(.*)>"
$java = $matches[0].Split('"')
$url = $java[2]
$webclient.DownloadFile($url, "C:\selenium\install_java.exe")
Start-Process "install_java.exe" -ArgumentList "/s" -Wait
$javaexe = Get-ChildItem -path 'C:\Program Files (x86)\Java' -filter java.exe -recurse
$newpath = $env:path + ';' + $javaexe.DirectoryName + '\;C:\selenium\'
$env:Path = $newpath
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name 'Path' -Value $newpath
$n = $webclient.DownloadString("http://nssm.cc/download")
$n -match "release(.*)zip"
$url = "http://nssm.cc/" + $matches[0]
$webclient.DownloadFile($url, "C:\selenium\nssm.zip")
$shell = new-object -com shell.application
$zip = $shell.NameSpace("C:\selenium\nssm.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
rm nssm.zip
mv nssm-* install_nssm
cp install_nssm/win64/nssm.exe .
[xml]$selenium = $webclient.DownloadString("https://selenium-release.storage.googleapis.com/")
[string]$jar = $selenium.ListBucketResult.Contents.Key | ? { $_ -match 'standalone' } | Select-Object -Last 1
$url = 'https://selenium-release.storage.googleapis.com/' + ($jar.Split('/')[0]) + '/' + ($jar.Split('/')[1])
$webclient.DownloadFile($url, "C:\selenium\selenium-server-standalone.jar")
nssm install seleniumhub java -jar C:\selenium\selenium-server-standalone.jar -role hub
Start-Service seleniumhub
$v = $webclient.DownloadString("https://www.virtualbox.org/wiki/Downloads")
$v -match "Windows hosts(.*)>"
$virtualbox = $matches[0].Split('"')
$url = $virtualbox[3]
$webclient.DownloadFile($url, "C:\selenium\install_virtualbox.exe")
Start-Process "install_virtualbox.exe" -ArgumentList "-s" -Wait
$v -match "Extension Pack(.*)>"
$extensionpack = $matches[0].Split('"')
$url = $extensionpack[3]
$f = [System.IO.Path]::GetFileName($url)
Start-Sleep -s 2
$webclient.DownloadFile($url, "C:\selenium\" + $f)
$newpath = $env:path + ';' + 'C:\Program Files\Oracle\VirtualBox\'
$env:Path += $newpath
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name 'Path' -Value $newpath
VBoxManage extpack install $f --replace
$chromedriverversion = $webclient.DownloadString("http://chromedriver.storage.googleapis.com/LATEST_RELEASE")
$url = "http://chromedriver.storage.googleapis.com/" + $chromedriverversion + "/chromedriver_win32.zip"
$webclient.DownloadFile($url, "C:\selenium\install_chromedriver.zip")
$zip = $shell.NameSpace("C:\selenium\install_chromedriver.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
rm install_chromedriver.zip
[string]$iedriver = $selenium.ListBucketResult.Contents.Key | ? { $_ -match 'IEDriverServer_Win32' } | Select-Object -Last 1
$url = 'https://selenium-release.storage.googleapis.com/' + ($iedriver.Split('/')[0]) + '/' + ($iedriver.Split('/')[1])
$webclient.DownloadFile($url, "C:\selenium\install_iedriver.zip")
$zip = $shell.NameSpace("C:\selenium\install_iedriver.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
rm install_iedriver.zip
$url = "http://dl.google.com/chrome/install/chrome_installer.exe"
$webclient.DownloadFile($url, "C:\selenium\install_chrome.exe")
$f = $webclient.DownloadString("https://www.mozilla.org/en-US/firefox/all/")
$f -match "https:\/\/download\.mozilla\.org\/(.*)os=win(.*)en-US"
$url = $matches[0] -replace "&amp;", "&"
$webclient.DownloadFile($url, "C:\selenium\install_firefox.exe")
$url = "https://github.com/tka/SeleniumBox/raw/master/deuac.iso"
$webclient.DownloadFile($url, "C:\selenium\deuac.iso")
$ip = get-WmiObject Win32_NetworkAdapterConfiguration|Where {$_.IPAddress.length -gt 1}
$hubip = $ip[1].IPAddress[0]
if (!$hubip) {
  sleep 30
  $ip = get-WmiObject Win32_NetworkAdapterConfiguration|Where {$_.IPAddress.length -gt 1}
  $hubip = $ip[1].IPAddress[0]
}

New-Item C:/selenium/node.json -type file
$nodejson = @"
{
  "capabilities":[
    {
      "platform": "WINDOWS",
      "browserName": "firefox",
      "maxInstances": 1,
      "seleniumProtocol": "WebDriver"
    },{
      "platform": "WINDOWS",
        "browserName": "chrome",
        "maxInstances": 1,
      "seleniumProtocol": "WebDriver"
    },{
      "platform": "WINDOWS",
      "browserName": "internet explorer",
      "version": "IE_VERSION",
      "maxInstances": 1,
      "seleniumProtocol": "WebDriver"
    }
  ],
  "configuration":{
    "hubHost":"$hubip",
    "host":"NODE_IP",
    "maxSession":5,
    "port":5555,
    "register":true,
    "registerCycle": 5000,
    "hubPort":4444,
    "nodeTimeout":120,
    "nodePolling":2000,
    "registerCycle":10000,
    "cleanUpCycle":2000,
    "timeout":30000
  }
}
"@

$nodejson > C:/selenium/node.json
Set-ExecutionPolicy RemoteSigned -Force
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
choco install python2
choco install graphicsmagick
choco install gow
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
choco install nodejs
npm install -g webdriverio
$webclient.DownloadFile('http://download.microsoft.com/download/9/6/4/96442E58-C65C-4122-A956-CCA83EECCD03/wdexpress_full.exe', "C:\selenium\install_cairo_dependency.exe")
Start-Process "install_cairo_dependency.exe" -ArgumentList "/q /norestart" -Wait
mkdir C:\GTK
cd C:\GTK
$webclient.DownloadFile("http://ftp.gnome.org/pub/gnome/binaries/win64/gtk+/2.22/gtk+-bundle_2.22.1-20101229_win64.zip", "C:\GTK\install_cairo.zip")
$zip = $shell.NameSpace("C:\GTK\install_cairo.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\GTK").copyhere($item) }
rm install_cairo.zip
$newpath = $env:path + ';' + 'C:\GTK\bin'
$env:Path += $newpath
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name 'Path' -Value $newpath
npm install -g node-gyp
$gypexe = Get-ChildItem -path 'C:\ProgramData\chocolatey\lib' -filter node-gyp.cmd -recurse
$nodepath = $gypexe[0].DirectoryName + '\node_modules'
$newpath = $env:path + ';' + $gypexe[0].DirectoryName + ';' + $nodepath
$env:Path = $newpath
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name 'Path' -Value $newpath
[Environment]::SetEnvironmentVariable("NODE_PATH", $nodepath, "Machine")
npm install -g webdrivercss
cd C:\selenium
mkdir ssh
New-Item C:/selenium/ssh/authorized_keys.txt -type file
$webclient.DownloadFile("http://www.powershellserver.com/downloads/free/NSX6-A/setup.exe", "C:\selenium\install_ssh.exe")
Start-Process "install_ssh.exe" -ArgumentList "/S /NoDemos /PATH=C:\selenium\ssh" -Wait
Set-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "RunAsAService" -Value "1"
New-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "EnableSCP" -PropertyType "DWord" -Value "1"
New-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "PasswordAuthEnabled" -PropertyType "DWord" -Value "0"
New-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "SSHPublicKeyEnable" -PropertyType "DWord" -Value "1"
New-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "PubKeyAuthEnabled" -PropertyType "DWord" -Value "1"
New-ItemProperty -Path "HKLM:\SOFTWARE\nsoftware\PowerShellServer" -Name "SSHPublicKeyFileName" -Value "C:\selenium\ssh\authorized_keys.txt"
C:\selenium\ssh\PowerShellServer.exe /servicestart
# https://www.modern.ie/en-us/virtualization-tools#downloads
$webclient.DownloadFile("https://az412801.vo.msecnd.net/vhd/VMBuild_20141027/VirtualBox/IE8/Windows/IE8.Win7.For.Windows.VirtualBox.zip", "C:\selenium\modernie_8.zip")
$zip = $shell.NameSpace("C:\selenium\modernie_8.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
$webclient.DownloadFile("https://az412801.vo.msecnd.net/vhd/VMBuild_20141027/VirtualBox/IE9/Windows/IE9.Win7.For.Windows.VirtualBox.zip", "C:\selenium\modernie_9.zip")
$zip = $shell.NameSpace("C:\selenium\modernie_9.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
$webclient.DownloadFile("https://az412801.vo.msecnd.net/vhd/VMBuild_20141027/VirtualBox/IE10/Windows/IE10.Win7.For.Windows.VirtualBox.zip", "C:\selenium\modernie_10.zip")
$zip = $shell.NameSpace("C:\selenium\modernie_10.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
$webclient.DownloadFile("https://az412801.vo.msecnd.net/vhd/VMBuild_20141027/VirtualBox/IE11/Windows/IE11.Win7.For.Windows.VirtualBox.zip", "C:\selenium\modernie_11.zip")
$zip = $shell.NameSpace("C:\selenium\modernie_11.zip")
foreach($item in $zip.items()) { $shell.Namespace("C:\selenium").copyhere($item) }
rm modernie_*.zip
$webclient.DownloadFile("https://bitbucket.org/uitsweb/selenium-grid-ec2/raw/HEAD/selenium.bat", "C:\selenium\selenium.bat")
# IE fix
echo 'mkdir C:/selenium' > iefix.ps1
echo '$webclient = New-Object System.Net.WebClient' >> iefix.ps1
echo '$webclient.DownloadFile("https://bitbucket.org/uitsweb/selenium-grid-ec2/raw/HEAD/iefix.reg", "C:\selenium\iefix.reg")' >> iefix.ps1
echo 'REG IMPORT C:\selenium\iefix.reg' >> iefix.ps1
# IE11 fix
echo 'New-Item -Path "HKLM:\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl" -Name FEATURE_BFCACHE' > ie11fix.ps1
echo 'New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BFCACHE" -Name "iexplore.exe" -PropertyType "DWord" -Value "0"' >> ie11fix.ps1
echo '$webclient = New-Object System.Net.WebClient' >> ie11fix.ps1
echo '$webclient.DownloadFile("http://download.microsoft.com/download/3/B/F/3BFE7A91-9E89-4508-A3E8-D55E92391E02/Windows7-KB2990999-x86.msu", "C:\selenium\install_ie11fix.msu")' >> ie11fix.ps1
echo 'wusa C:\selenium\install_ie11fix.msu /quiet /norestart' >> ie11fix.ps1
# Selenium node setup
echo 'netsh advfirewall set allprofiles state off' > nodesetup.ps1
echo 'Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings" -Name "CertificateRevocation" -Value "0"' >> nodesetup.ps1
echo 'cp "D:\install_java.exe" "C:\selenium\"' >> nodesetup.ps1
echo 'cp "D:\install_chrome.exe" "C:\selenium\"' >> nodesetup.ps1
echo 'cp "D:\install_firefox.exe" "C:\selenium\"' >> nodesetup.ps1
echo 'Start-Process "C:/selenium/install_java.exe" -ArgumentList "/s" -Wait' >> nodesetup.ps1
echo 'Start-Process "C:/selenium/install_chrome.exe" -ArgumentList "/silent /install" -Wait' >> nodesetup.ps1
echo 'Start-Process "C:/selenium/install_firefox.exe" -ArgumentList "-ms" -Wait' >> nodesetup.ps1
echo 'Start-Sleep -s 30' >> nodesetup.ps1
echo 'cp "D:\selenium-server-standalone.jar" "C:\selenium\"' >> nodesetup.ps1
echo 'cp "D:\node.json" "C:\selenium\"' >> nodesetup.ps1
echo 'cp "D:\IEDriverServer.exe" "C:\Windows\system32\"' >> nodesetup.ps1
echo 'cp "D:\chromedriver.exe" "C:\Windows\system32\"' >> nodesetup.ps1
echo 'cp "D:\selenium.bat" "C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Startup/"' >> nodesetup.ps1
echo '$javaexe = Get-ChildItem -path "C:\Program Files\Java" -filter java.exe -recurse' >> nodesetup.ps1
echo '$newpath = $javaexe.DirectoryName + "\;C:\selenium\;" + $env:path' >> nodesetup.ps1
echo '$env:Path = $newpath' >> nodesetup.ps1
echo 'Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Session Manager\Environment" -Name "Path" -Value $newpath' >> nodesetup.ps1
echo 'Stop-Process -Name Explorer -Force' >> nodesetup.ps1
echo '$ip = get-WmiObject Win32_NetworkAdapterConfiguration|Where {$_.IPAddress.length -gt 1}' >> nodesetup.ps1
echo '$nodeip = $ip[1].IPAddress[0]' >> nodesetup.ps1
echo '$ieversion = (Get-ItemProperty -Path "HKLM:\Software\Microsoft\Internet Explorer" -Name "Version").Version' >> nodesetup.ps1
# Arghh... why is Microsoft so stupid with their IE versioning?!  https://support.microsoft.com/kb/969393
echo '$ieversion = $ieversion.replace("9.10.","10.0.").replace("9.11.","11.0.")' >> nodesetup.ps1
echo '$nodestr = [string](gc C:\selenium\node.json)' >> nodesetup.ps1
echo '$nodestr.replace("NODE_IP",$nodeip).replace("IE_VERSION",$ieversion.Split(".")[0])|sc C:\selenium\node.json' >> nodesetup.ps1
echo 'Start-Process "C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Startup/selenium.bat"' >> nodesetup.ps1
echo 'cscript C:\Windows\System32\slmgr.vbs /ato' >> nodesetup.ps1
echo 'SchTasks /Create /SC DAILY /MO 85 /TN "Re-activate Windows" /TR "cscript C:\Windows\System32\slmgr.vbs /rearm" /ST 02:00' >> nodesetup.ps1
echo 'SchTasks /Create /SC DAILY /MO 85 /TN "Restart Windows" /TR "shutdown.exe /r /t 00" /ST 03:00' >> nodesetup.ps1
echo 'start-process iexplore; start-process firefox; start-process chrome; sleep 60; get-process iexplore | stop-process; get-process firefox | stop-process; get-process chrome | stop-process;' >> nodesetup.ps1
$iearray = @("IE8", "IE9", "IE10", "IE11")
foreach ($ieinstance in $iearray) {
  VBoxManage import "C:\selenium\$ieinstance - Win7.ova" --vsys 0 --memory 3072 --vsys 0 --unit 11 --disk "C:\selenium\$ieinstance.vmdk" --vsys 0 --vmname "$ieinstance"
  VBoxManage modifyvm "$ieinstance" --vram 128 --nic1 nat --nic2 hostonly --hostonlyadapter2 'VirtualBox Host-Only Ethernet Adapter'
  VBoxManage sharedfolder add "$ieinstance" --name "selenium" --hostpath "C:\selenium" --automount
  VBoxManage snapshot "$ieinstance" take "$ieinstance-snapshot"
  VBoxManage storageattach "$ieinstance" --storagectl "IDE" --port 1 --device 0 --type dvddrive --medium "C:\selenium\deuac.iso"
  VBoxManage startvm "$ieinstance" --type headless
  Start-Sleep -s 30
  VBoxManage storageattach "$ieinstance" --storagectl "IDE" --port 1 --device 0 --type dvddrive --medium none
  VBoxManage startvm "$ieinstance" --type headless
  Start-Sleep -s 120
  VBoxManage guestcontrol "$ieinstance" execute --image cmd.exe --username 'IEUser' --password 'Passw0rd!' -- /c start powershell -ExecutionPolicy ByPass -File D:/iefix.ps1
  If($ieinstance -eq "IE11")
  {
    Start-Sleep -s 15
    VBoxManage guestcontrol "$ieinstance" execute --image cmd.exe --username 'IEUser' --password 'Passw0rd!' -- /c start powershell -ExecutionPolicy ByPass -File D:/ie11fix.ps1
  }

  Start-Sleep -s 30
  VBoxManage guestcontrol "$ieinstance" execute --image cmd.exe --username 'IEUser' --password 'Passw0rd!' -- /c start powershell -ExecutionPolicy ByPass -File D:/nodesetup.ps1
  rm "$ieinstance*.ova"
}

echo 'VBoxManage startvm IE8 --type headless' > runie.ps1
echo 'VBoxManage startvm IE9 --type headless' >> runie.ps1
echo 'VBoxManage startvm IE10 --type headless' >> runie.ps1
echo 'VBoxManage startvm IE11 --type headless' >> runie.ps1
nssm install runie powershell -command C:\selenium\runie.ps1
$webclient.DownloadFile("https://bitbucket.org/uitsweb/selenium-grid-ec2/raw/HEAD/webdriverio_test.js", "C:/selenium/webdriverio_test.js")
$webclient.DownloadFile("https://bitbucket.org/uitsweb/selenium-grid-ec2/raw/HEAD/webdrivercss_test.js", "C:/selenium/webdrivercss_test.js")
