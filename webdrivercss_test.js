var webdriverio = require('webdriverio');
var webdrivercss = require('webdrivercss');
var capabilities = {
    host: 'localhost',
    port: 4444,
    desiredCapabilities: {
        browserName: 'chrome',
        platform: 'WINDOWS',
        initialBrowserUrl: ''
    }
};
var client = webdriverio.remote(capabilities);
webdrivercss.init(client, {screenshotRoot:'screenshots-raw', failedComparisonsRoot:'screenshots-diff'});
var url = 'http://coolepochcountdown.com/';
var id = url.replace(/^https?:/,'').replace(/\./g,'_').replace(/[^-,^_,'A-Za-z0-9]+/g,'');
var name = capabilities.desiredCapabilities.browserName + '_' + (new Date).getTime();
client
    .init()
    .url(url)
    .webdrivercss(id, {name:name, elem:'#timer'})
    .webdrivercss(id, {name:name, elem:'#timer'}, function(err, res){ console.log('diff result:', res[name][0].diffPath); })
    .end();
    